import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FeedbacksService {
  // public userDetail =[
  //   {
  //     id:1,
  //     img:'http://www.personalbrandingblog.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640-300x300.png',
  //     name:'Shubham'
  //   },
  //   {
  //     id:2,
  //     img:'https://www.thehindu.com/sci-tech/technology/internet/article17759222.ece/alternates/FREE_660/02th-egg-person',
  //     name:'Soni'
  //   },
  //   {
  //     id:3,
  //     img:'https://www.thehindu.com/sci-tech/technology/internet/article17759222.ece/alternates/FREE_660/02th-egg-person',
  //     name:'Great'
  //   }
  // ]
  public back = { feed: [] };

  public msg = {
    rid: '',
    message: '',
    flag: ''
  };
  baseUrl = ' http://92d99cf4.ngrok.io/';

  constructor(private link: HttpClient) {}
  // getFeed(msg){
  //   console.log(this.msg)

  // }
  showFeedback(): Observable<any> {
    const data = JSON.parse(window.localStorage.getItem('userData'));
    // let headers= new HttpHeaders().set('authorization',data.Token)
    console.log(data.id);
    const id = data.id;
    return this.link.get(this.baseUrl + 'Feedback/' + id);
  }
  addFeedbackUser(): Observable<any> {
    const data = JSON.parse(window.localStorage.getItem('userData'));
    // let headers= new HttpHeaders().set('authorization',data.Token)

    return this.link.get(this.baseUrl + 'Feedback');
  }
  addFeeback(feeds): Observable<any> {
    console.log(feeds);
    const data = JSON.parse(window.localStorage.getItem('userData'));
    const headers = new HttpHeaders().set('authorization', data.Token);
    return this.link.post(this.baseUrl + 'Feedback', feeds);
  }
}
