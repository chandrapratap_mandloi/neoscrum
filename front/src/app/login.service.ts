import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  // public log={
  //   email:'',
  //   password:''
  // }
  baseUrl = '  http://92d99cf4.ngrok.io/';
  constructor(private link: HttpClient) {}

  getLog(data): Observable<any> {
    console.log(data);
    return this.link.post(this.baseUrl + 'register', data);
  }
  getfeeback(): Observable<any> {
    const data = JSON.parse(window.localStorage.getItem('userData'));
    // let headers= new HttpHeaders().set('authorization',data.Token)
    console.log(data.Token);
    return this.link.get(this.baseUrl + 'Dashbord');
  }
}
