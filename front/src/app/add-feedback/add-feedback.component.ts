import { Component, OnInit } from '@angular/core';
import { FeedbacksService } from '../feedbacks.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-feedback',
  templateUrl: './add-feedback.component.html',
  styleUrls: ['./add-feedback.component.css']
})
export class AddFeedbackComponent implements OnInit {
  public getFeed = JSON.parse(localStorage.getItem('userData'));
  public imga = ' http://92d99cf4.ngrok.io/' + this.getFeed.Image;
  // public userDetail=this.feed.userDetail;
  feedback = this.feed.back;
  feedbacks;
  feedcards = [];
  //   {feed:[]};
  // let count = 0 ;
  public msg = this.feed.msg;

  public back = this.feed.back;

  public getTask = JSON.parse(localStorage.getItem('Data'));
  constructor(
    // private spinner: NgxSpinnerService,
    private feed: FeedbacksService,
    private route: Router,
    private snackBar: MatSnackBar
  ) {}
  select = null;
  ngOnInit() {
    // this.spinner.show();
    this.feed.addFeedbackUser().subscribe(item => {
      if (item.success === 'false') {
        // this.spinner.hide();
      } else {
        // this.spinner.hide();
        this.feedcards = item;
        console.log('this.feedcards', this.feedcards);
      }
    });
    // console.log(this.feed.userDetail);
  }
  submitFeed(event, i, detail) {
    console.log('data', detail);
    // this.select=event.target;
    // console.log(this.select);
    // console.log(a);
    // this.select.disabled=true;
    // console.log(message.value.length)
    console.log(i);
    this.msg.rid = detail.d_id;
    this.msg.message = this.back.feed[i];
    this.msg.flag = 'true';
    // this.msg.sender=this.getFeed.id;
    console.log(this.msg);
    const text = 'text' + i;
    const x = document.getElementById(text).nodeValue;
    console.log(x);
    // this.feed.addFeeback(this.msg);
    if (
      this.msg.message === ' ' ||
      this.msg.message === undefined ||
      this.msg.message === null
    ) {
      alert('please write something');
    } else {
      // this.spinner.show();
      this.feed.addFeeback(this.msg).subscribe(item => {
        if (item.Success === false) {
          // this.spinner.hide();
          console.log('submit feedback error');
        } else {
          // this.spinner.hide();
          this.feedbacks = item;
          console.log('after submit success ful', this.feedbacks);
          this.snackBar.open('Feedback Sent', '', {
            duration: 2000
          });
          const a = 'id' + i;
          const b = 'text' + i;
          document.getElementById(a).setAttribute('disabled', 'disabled');
          document.getElementById(b).setAttribute('disabled', 'disabled');
          // this.spinner.show();
          // this.feed.addFeedbackUser().subscribe( item => {
          //   if (item.success === 'false') {
          //     this.spinner.hide();
          //   } else {
          //     this.spinner.hide();
          //     this.feedcards = item;
          //     console.log('this.feedcards', this.feedcards);
          //   }
          // });
        }
      });
    }
  }
  // openSnackBar() {
  //   this.snackBar.open('Feedback Sent', ', {
  //     duration: 3000,
  //   });
  // }
  textAreaAdjust(i) {
    // debugger;
    // console.log(o)
    // count++;
    const id = 'text' + i;
    const a = document.getElementById(id);
    a.style.height = '1px';
    a.style.height = 25 + a.scrollHeight + 'px';
  }
  logout() {
    localStorage.clear();
    this.route.navigate(['/login']);
  }
}
