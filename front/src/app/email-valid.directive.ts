import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';
// import { LoginPanelComponent } from '/app/login-panel';
import { ActivatedRoute, Router } from '@angular/router';
// import { LoginPanelComponent } from './login-panel/login-panel.component';
// import }{}
@Directive({
  selector: '[appEmailValid]'
})
export class EmailValidDirective implements OnInit {
  a = null;
  constructor(private eRef: ElementRef, private route: Router) {
    // console.log(this.a);
  }
  ngOnInit() {
    // this.eRef.nativeElement.style.color='red';
  }
  @HostListener('blur') onblur() {
    this.a = this.eRef.nativeElement.value;
    console.log(this.a);
    const email = /^([a-zA-Z0-9\._]+)@([a-zA-z0-9-]+).([a-z]{2,8})(.[a-z]{2})?$/;

    if (email.test(this.a)) {
      this.route.navigate(['/dashboard']);
      console.log('correct');
      return true;
    } else {
      console.log('errror');
      return false;
    }
  }
}
