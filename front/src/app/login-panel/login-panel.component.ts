import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../login.service';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
// import { start } from 'repl';
@Component({
  selector: 'app-login-panel',
  templateUrl: './login-panel.component.html',
  styleUrls: ['./login-panel.component.css']
})
export class LoginPanelComponent implements OnInit {
  hide = true;
  time: any;
  log = {
    e_mail: '',
    password: ''
  };
  n = Date.now();
  // public retainTasks = JSON.parse(localStorage.getItem('Data'));
  constructor(
    private spinner: NgxSpinnerService,
    private act: ActivatedRoute,
    private route: Router,
    public logg: LoginService
  ) {}

  ngOnInit() {
    // this.spinner.show();
    setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
    // this.start();
    console.log(this.n);
    this.time = moment('20190402', 'YYYYMMDD').fromNow();
  }
  register() {
    // this.route.navigate(['/dashboard']);
    // this.spinner.show();
    if (this.emaill() === false || this.pass() === false) {
      return false;
    } else {
      this.logg.getLog(this.log).subscribe(item => {
        // debugger;
        console.log('login', item);
        if (item.success === 'false') {
          this.spinner.hide();
          alert(item.message);
          alert(item.Err[0].message);
        } else {
          console.log('true', item.message);

          window.localStorage.setItem('userData', JSON.stringify(item));
          this.route.navigate(['/dashboard']);
          this.spinner.hide();
        }
      });

      // let data = JSON.parse(window.localStorage.getItem('userData'))
      // if(data.message=='Password not Match')
      // {
      //   alert('Incorrect Password')
      // }
      // else
      // {
      //
      // }

      // const mail=this.log.dev_email;
      // this.retainTasks.push({ id:mail});
      // localStorage.setItem('Data', JSON.stringify(retainTasks));
    }
  }
  start() {
    const getTasks = window.localStorage.getItem('userData');
    if (getTasks === null || getTasks === undefined || getTasks === '') {
      localStorage.setItem('userData', JSON.stringify([]));
    }
  }
  emaill() {
    const mail = this.log.e_mail;
    // console.log(mail);
    const email = /^(?!\.)(?!.*\.$)(?!.*?\.\.)^([a-zA-Z0-9\._]+)@([a-zA-z0-9-]+).([a-z]{2,8})(.[a-z]{2})?$/;

    if (email.test(mail)) {
      // console.log('correct');
      return true;
    } else {
      alert('Wrong Email');
      //  console.log('errror')
      return false;
    }
  }
  pass() {
    const pass = this.log.password;
    if (pass === '' || pass === null || pass === undefined) {
      return false;
    }
  }
}
