import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private route: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (state.url.includes('/login')) {
      if (JSON.parse(localStorage.getItem('userData'))) {
        this.route.navigate(['/dashboard']);
        return false;
      } else {
        return true;
      }
    } else if (state.url.includes('/dashboard')) {
      if (JSON.parse(localStorage.getItem('userData'))) {
        return true;
      } else {
        this.route.navigate(['/login']);

        return false;
      }
    } else {
      // alert('not login')
      console.log(state.url);
    }
    // console.log(state.url.includes('/login'));
    // console.log(JSON.parse(localStorage.getItem('userData')).token)
    // return false;
  }
}
