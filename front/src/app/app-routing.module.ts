import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { LoginPanelComponent } from './login-panel/login-panel.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddFeedbackComponent } from './add-feedback/add-feedback.component';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { AuthGuard } from './auth.guard';
// import { DashGuard } from './dash.guard';
import { HashLocationStrategy } from '@angular/common';
// HashLocationStrategyStrategy
const routes: Routes = [
  { path: 'admin', component: AdminPanelComponent },
  {
    path: 'login',
    component: LoginPanelComponent,
    canActivate: [AuthGuard]
  },
  // {
  //     path:'login', component:LoginPanelComponent,
  //   },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  { path: 'addFeedbacks', component: AddFeedbackComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
  // canActivate:[DashGuard]
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
