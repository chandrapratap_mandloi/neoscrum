import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

export class HttpErrorInterceptor implements HttpInterceptor {
  // constructor(private spinner: NgxSpinnerService) {}
  public getFeed = JSON.parse(localStorage.getItem('userData'));
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.getFeed === null) {
      console.log(JSON.parse(localStorage.getItem('userData')));
      // debugger;
    } else {
        request = request.clone({
          setHeaders: {
            Authorization: `${this.getFeed.token}`
          }
        });
    }
    return next.handle(request).pipe(
      retry(1),
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Error: ${error.error.message}`;
        } else {
          // server-side error
          errorMessage = `Error Code: ${error.status}\nMessage: ${
            error.message
          }`;
        }
        console.log(errorMessage);
        // this.spinner.hide();
        // this.spinner.hide();
        return throwError(errorMessage);
      })
    );

  }
}
