import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {
  public new = {
    name: '',
    e_mail: '',
    multer: ''
  };
  select = null;
  public new1 = [];
  formData = new FormData();
  constructor(private user: UserService, private route: Router) {}

  ngOnInit() {
    // this.new=this.user.getUser();
    // // this.new=this.user.new;
    // console.log(this.new);
  }
  selectedImg(event) {
    this.select = event.target.files[0];
    this.formData.append('product_image', this.select);
    // this.user.getUser(this.new);
    // const decode=window.btoa(this.select);
    // console.log(decode);
    // this.new.attachment=this.select;
    console.log(this.formData);
  }
  register() {
    // console.log(this.new);
    // this.new=this.user.getUser();
    // this.new=this.user.new;
    // console.log(this.new)
    if (this.name() === false || this.email() === false) {
    } else {
      this.formData.append('name', this.new.name);
      this.formData.append('e_mail', this.new.e_mail);
      this.user.getUser(this.formData).subscribe((item) => {
        alert(item.message);
      });
      this.route.navigate(['/login']);
    }
  }
  name() {
    const dName = this.new.name;
    if (dName === '' || dName === null || dName === undefined) {
      alert('Name Required');
      return false;
    }
  }
  email() {
    const mail = this.new.e_mail;
    console.log(mail);
    const email = /^(?!\.)(?!.*\.$)(?!.*?\.\.)^([a-zA-Z0-9\._]+)@([a-zA-z0-9-]+).([a-z]{2,8})(.[a-z]{2})?$/;

    if (email.test(mail)) {
      console.log('correct');
      return true;
    } else {
      alert('Incorrect Email');
      console.log('errror');
      return false;
    }
  }
}
