const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const db = require('./queries')
const profile = require('./profile')
const generator = require('generate-password');
const multer = require('multer')
const cron=require('node-cron')
const cors = require('cors');
const port = 3009
const router = express.Router();
const jwt = require('jsonwebtoken');

// Cron Job Send E-Mail on fixed intervals
// cron.schedule('*/10 * * * *', () => {
//   db.createcron();
// });


let verifyToken = function(req,res,next){
  const bearerHeader = req.headers['authorization'];
  const token = bearerHeader;
  req.token = token;
  jwt.verify(token, 'privateKey', function(err, authData) {
    if(err) {
      return res.sendStatus(403);
    }
    else{
      req.body.id = authData.id;
      next();
    }
  });
  }
  // router.get('/getProduct', verifyToken, getProduct);

app.use(cors());
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
const storage = multer.diskStorage({
  destination: function (request, file, cb) {
      cb(null, './uploads/')
  },
  filename: function (request, file, cb) {
      cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
  }
})
let upload = multer({ storage: storage });
const publicDir = require('path').join(__dirname,'/uploads');
app.use(express.static(publicDir));

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

// API forUser Registration and And Genrating Password Automatically
app.post('/users',upload.single('product_image'), db.createUser)
// app.post('/users',db.createUser)

// API for selecting receiver
// app.get('/Feedback/:id',verifyToken, db.getFeedback)
app.get('/Feedback',verifyToken, db.getFeedback)

//API Saving Feedback of users into table
app.post('/Feedback', verifyToken,db.createFeedback)

//API for User Authentication
app.post('/register',db.register)

//API forCreating list of Feedback Sender and Feedback Receiver 
//Automatic Genrating E-Mail and to sender they recieve mail including name of receiver
app.get('/cron', db.createcron)

// API For Showing Feedback On Dashboard Of User

app.get('/Dashbord', verifyToken,db.Dashbord)
app.post('./profile',profile);

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})