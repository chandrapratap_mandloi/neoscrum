const generator = require('generate-password');
const bcrypt = require('bcrypt');
const passwordHash = require('password-hash');
const express = require('express');
const cors = require('cors');
const router = express.Router();
const multer = require('multer');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const sendmail = require('sendmail')()
const Pool = require('pg').Pool
const pool = new Pool({
  user: "postgres",
  password: "p@ssw0rd",
  host: "localhost",
  port: 5432,
  database: "try"
})


pool.connect()
// User Registration and And Genrating Password Automatically
const createUser = (request, response) => {
  const { name, e_mail, password, image_path } = request.body
  const array =
  [
    "Aardvark",
    "Albatross",
    "Alligator",
    "Alpaca",
    "Ant",
    "Anteater",
    "Antelope",
    "Ape",
    "Armadillo",
    "Donkey",
    "Baboon",
    "Badger",
    "Barracuda",
    "Bat",
    "Bear",
    "Beaver",
    "Bee",
    "Bison",
    "Boar",
    "Buffalo",
    "Butterfly",
    "Camel",
    "Capybara",
    "Caribou",
    "Cassowary",
    "Cat",
    "Caterpillar",
    "Cattle",
    "Chamois",
    "Cheetah",
    "Chicken",
    "Chimpanzee",
    "Chinchilla",
    "Chough",
    "Clam",
    "Cobra",
    "Cockroach",
    "Cod",
    "Cormorant",
    "Coyote",
    "Crab",
    "Crane",
    "Crocodile",
    "Crow",
    "Curlew",
    "Deer",
    "Dinosaur",
    "Dog",
    "Dogfish",
    "Dolphin",
    "Dotterel",
    "Dove",
    "Dragonfly",
    "Duck",
    "Dugong",
    "Dunlin",
    "Eagle",
    "Echidna",
    "Eel",
    "Eland",
    "Elephant",
    "Elk",
    "Emu",
    "Falcon",
    "Ferret",
    "Finch",
    "Fish",
    "Flamingo",
    "Fly",
    "Fox",
    "Frog",
    "Gaur",
    "Gazelle",
    "Gerbil",
    "Giraffe",
    "Gnat",
    "Gnu",
    "Goat",
    "Goldfinch",
    "Goldfish",
    "Goose",
    "Gorilla",
    "Goshawk",
    "Grasshopper",
    "Grouse",
    "Guanaco",
    "Gull",
    "Hamster",
    "Hare",
    "Hawk",
    "Hedgehog",
    "Heron",
    "Herring",
    "Hippopotamus",
    "Hornet",
    "Horse",
    "Human",
    "Hummingbird",
    "Hyena",
    "Ibex",
    "Ibis",
    "Jackal",
    "Jaguar",
    "Jay",
    "Jellyfish",
    "Kangaroo",
    "Kingfisher",
    "Koala",
    "Kookabura",
    "Kouprey",
    "Kudu",
    "Lapwing",
    "Lark",
    "Lemur",
    "Leopard",
    "Lion",
    "Llama",
    "Lobster",
    "Locust",
    "Loris",
    "Louse",
    "Lyrebird",
    "Magpie",
    "Mallard",
    "Manatee",
    "Mandrill",
    "Mantis",
    "Marten",
    "Meerkat",
    "Mink",
    "Mole",
    "Mongoose",
    "Monkey",
    "Moose",
    "Mosquito",
    "Mouse",
    "Mule",
    "Narwhal",
    "Newt",
    "Nightingale",
    "Octopus",
    "Okapi",
    "Opossum",
    "Oryx",
    "Ostrich",
    "Otter",
    "Owl",
    "Oyster",
    "Panther",
    "Parrot",
    "Partridge",
    "Peafowl",
    "Pelican",
    "Penguin",
    "Pheasant",
    "Pig",
    "Pigeon",
    "Pony",
    "Porcupine",
    "Porpoise",
    "Quail",
    "Quelea",
    "Quetzal",
    "Rabbit",
    "Raccoon",
    "Rail",
    "Ram",
    "Rat",
    "Raven",
    "Red deer",
    "Red panda",
    "Reindeer",
    "Rhinoceros",
    "Rook",
    "Salamander",
    "Salmon",
    "Sand Dollar",
    "Sandpiper",
    "Sardine",
    "Scorpion",
    "Seahorse",
    "Seal",
    "Shark",
    "Sheep",
    "Shrew",
    "Skunk",
    "Snail",
    "Snake",
    "Sparrow",
    "Spider",
    "Spoonbill",
    "Squid",
    "Squirrel",
    "Starling",
    "Stingray",
    "Stinkbug",
    "Stork",
    "Swallow",
    "Swan",
    "Tapir",
    "Tarsier",
    "Termite",
    "Tiger",
    "Toad",
    "Trout",
    "Turkey",
    "Turtle",
    "Viper",
    "Vulture",
    "Wallaby",
    "Walrus",
    "Wasp",
    "Weasel",
    "Whale",
    "Wildcat",
    "Wolf",
    "Wolverine",
    "Wombat",
    "Woodcock",
    "Woodpecker",
    "Worm",
    "Wren",
    "Yak",
    "Zebra"
  ]
  const randomValue = array[Math.floor(Math.random() * array.length)];
  const passwrd = randomValue + 123;
  const saltRounds = 10
  const hash = passwrd
  const salt = bcrypt.genSaltSync(saltRounds)
  const passwordHsh = bcrypt.hashSync(hash, salt)
    if (request.body === {} || request.body === '') 
    return response.status(505).json({ statusCode: 505, message: 'updateAdd object can not be empty', data: '' });
    request.body['product_image'] = request.file.originalname;
    const schema = Joi.object().keys({
    u_name: Joi.string().regex(/^[^-\s][a-zA-Z\s-]+$/).required(),
    u_email: Joi.string().email({ minDomainAtoms: 2 }).required()
})

Joi.validate({ u_name: name, u_email: e_mail }, schema, function (err, res) { 
if(err)
response.json({success:'false', Err:err.details})
else{
  pool.query('INSERT INTO login (name, e_mail,password,multer) VALUES ($1, $2,$3,$4)', [name, request.body.e_mail, passwordHsh,request.file.filename], (error, results) => {
    if (error) {response.status(404).json({status_code:404,success:'false', message:'E-Mail Already Exists'})}
    else{
      const output = `
      <h3> Hi ${request.body.name} Your Login Details are: </h3>
      <ul>
        <li>Name: ${request.body.name}</li>
        <li>Email: ${request.body.e_mail}</li>
        <li>Password: ${passwrd}</li>
        <li><p>Login Link: <a href="http://94fb20d6.ngrok.io/login">Click Here</a></p></li>
      </ul>`;
    sendmail({
      from: 'mandloi.parth@gmail.com',
      to: request.body.e_mail,
      cc:'mandloi.parth@gmail.com',
      subject: 'Login Details',
      html: output
    },
      function (err) {
        console.log('error mail', err)
      })
      response.status(200).json({status_code:200,success:'true', message: "User Registered Successfully" , name:request.body.name})
    }
  })
}
})
} 

// API For Showing Feedback On Dashboard Of User
const Dashbord = (request, response) => {
  pool.query('select * from login', (error,results) =>{
    if(error){response.status(400).json({status_code:400,success:'false', Err:error.details})}
    else{
      pool.query('select message,f_d from feedback where id=($1)',[request.body.id], (error,res) => {
        if(error){ response.status(400).json({status_code:400,success:'false', err:error.details})}
        else{
          const feedback_array=[]
          for(let i=0;i<res.rows.length;i++)
          {
            feedback_array.push(res.rows[i]); 
          }
          response.status(200).json({status_code:200, success:'true',message:'Your Feedbacks Are', feedback:feedback_array})
        }
      })
    }
  })
}

// User Authentication
const register = (request, response) => {
  const { e_mail, password } = request.body
  const schema = Joi.object().keys({
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    u_password: Joi.string().required()
})
Joi.validate({ email: e_mail, u_password: password }, schema, function (err, res) { 
  if(err)
  response.json({success:'false', err:err.details})
  else{
    pool.query('select * from login where e_mail=($1)', [e_mail], (error, results) => {
      if (error) { response.status(400).json({status_code:400,success:'false', err:err.details}) }
      if (results.rows.length == 0) {
        response.status(200).json({status_code:200,success:'false', message: "Enter Correct E-Mail" })
      }
        else {
          if (bcrypt.compareSync(request.body.password, results.rows[0].password)) {
            jwt.sign({ email: request.body.e_mail,id:results.rows[0].id }, 'privateKey', function(err, token) {
              response.status(200).json({status_code:200,success:'true', Message:"Login Successfully",id: request.body.id, Name: results.rows[0].name, Email: results.rows[0].e_mail, Image: results.rows[0].multer,token: token})
            });
          } else {
            response.status(200).json({status_code:200,success:'false', message: "Password not Match" })
          }
        }
    })
  }
})
}

//Saving Feedback of users into table
const createFeedback  = (request, response) => {
  const { rid,message } = request.body
  const schema = Joi.object().keys({
    receiver_id: Joi.number().required(),
    feedback: Joi.string().min(1).max(200).required()
  })
  Joi.validate({  receiver_id: rid, feedback: message }, schema, function (err, results) { 
  if(err){ return  response.json({success:'false', Err:err.details})  }
  else{
    pool.query('select * from cron where id=$1 and d_id=$2',[request.body.id,rid], (err,results) => {
      if(err){return response.status(400).json({status_code:400,success:'false'})}
      else{
        if(results.rows[0].flags==true)
        {
         return response.status(400).json({status_code:400,success:'false',message:'You Have Already Given Feedback'})
        }
        else{
          pool.query('insert into feedback(id,message) values ($1,$2)',[rid,message], (error, results) => {
            if (error) {response.status(400).json({status_code:400,success:'false'})}
            else{
                pool.query('update cron set flags=true where id=$1 and d_id=$2', [request.body.id, rid], (err,results) => {
                if(err) {response.status(400).json({status_code:400,success:'false'})}
                else{
                  response.status(200).json({status_code:200,success:'true',message:'Feedback Inserted In Database'}) 
                }
                })
            }
          })
        }
      }
    })
  }
});
}

// for selecting receiver
const getFeedback = (request, response) => {
  pool.query('select name,d_id,multer,flags from cron where id = $1 and flags=$2',[request.body.id,false], (error, results) => {
    if (error) { response.status(400).json({status_code:400,success:'false', Err:error.details}) }
    else {
      response.status(200).json({status_code:200,success:'true',results:results.rows})
    }
  })
}

//Creating list of Feedback Sender and Feedback Receiver 
//Automatic Genrating E-Mail and to sender they recieve mail including name of receiver
const createcron = (request, response) => {
  pool.query('truncate cron', (error, res) => {
    if (error) { response.json({success:'false', err:error.details}) }})
  pool.query('SELECT e_mail,id,name,multer FROM login ', (error, results) => {
    if (error) { throw error }
    else {
      let s_id = [];
      do {
        let newItems = [];
        let r_name = [];
        let multer = [];
        let randomNum = Math.floor(Math.random() * results.rows.length);
        if (s_id.indexOf(randomNum) === -1) {
            let sender_id = results.rows[randomNum];
          s_id.push(randomNum);
          do {
            let idx = Math.floor(Math.random() * results.rows.length);
            if (sender_id.id !== results.rows[idx].id && newItems.indexOf(idx) === -1) {
              const r_id = results.rows[idx].id;
              newItems.push(idx);
              r_name.push(results.rows[idx].name)
              multer.push(results.rows[idx].multer)
              pool.query('insert into cron values($1,$2,$3,$4)', [sender_id.id, results.rows[idx].name, results.rows[idx].id, results.rows[idx].multer], (error, results) => {
                if (error) {throw error}
              })
            }
          }
          while (newItems.length <= 2);
        }
        let sender_id = results.rows[randomNum];
        const output = `
        <h3> Hey! ${sender_id.name} You Have to give feedback to following Developers</h3>
        <ul>
          <li>Name: ${r_name[0]}</li>
          <li>Name: ${r_name[1]}</li>
          <li>Name: ${r_name[2]}</li>
          <li><p>Login Link: <a href="https://94fb20d6.ngrok.io/login">Click Here</a></p></li>
        </ul>`;
     sendmail({
       from: 'mandl@gmil.com',
      //  to: sender_id.e_mail,
      to:'',
       cc:'mandloi.parth@gmail.com',
       subject: 'Feedback Details',
       html: output
     },
       function (err) {
         console.log(err)
       }
     )
      }
      while (s_id.length <= 3);
    }
  })
  response.status(200).json({status_code:200,success:'true',message:'Feedback Details Calculated and email sent to user containing name of receiver'})
}
module.exports = {
  createUser,
  getFeedback,
  createFeedback,
  createcron,
  register,
  Dashbord
}